<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return $category;
    }

    public function getUserCategorys($id){
        $category = Category::where("user_id",$id)->get();
        return $category;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|string|max:255',
            'category_image' => 'file|image|required',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $Category = new Category;
        $path = $request->file('category_image')->store('category', 'public');
        $Category ->category_name = $request->category_name;
        $Category->category_image = "storage/".$path;
        $Category ->user_id = $request->user_id;

        $Category->save();

        $result = array("status"=>200,"message"=>"Category Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $Category = Category::find($id);
        $Category ->category_name = $request->category_name;


        $Category->save();

        $result = array("status"=>200,"message"=>"Category Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }
}
