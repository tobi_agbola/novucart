<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $option = Option::all();
        return $option;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'option_content' => 'required|string',
            'question_id' => 'required|string',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $option = new Option;
        $option ->option_content = $request->option_content;
        $option ->question_id = $request->question_id;
        $option ->user_id = $request->user_id;

        $option->save();

        $result = array("status"=>200,"message"=>"Option Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $option = Option::find($id);
        return $option;
    }

    public function getOptionsFromQuestionId($id){
        $option = Option::where('question_id','=',$id)->get();
        return $option;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'option_content' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $option = Option::find($id);
        $option ->option_content = $request->option_content;

        $option->save();

        $result = array("status"=>200,"message"=>"Option Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $option = Option::find($id);
        $option->delete();
    }
}
