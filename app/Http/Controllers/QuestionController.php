<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = Question::all();
        return $question;
    }

    public function getUserQuestions($id){
        $question = Question::where("user_id",$id)->get();
        return $question;
    }
    public function getSurveyQuestions($id){
        $question = Question::where("survey_id",$id)->get();
        return $question;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question_content' => 'required|string',
            'answer_type' => 'required|string|max:255',
            'survey_id' => 'required|string',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $product = new Question;
        $product ->question_content = $request->question_content;
        $product ->answer_type = $request->answer_type;
        $product ->survey_id = $request->survey_id;
        $product ->user_id = $request->user_id;

        $product->save();

        $result = array("status"=>200,"message"=>"Question Added", "data"=>"success" );
         return json_encode($result);
    }

    public function getQuestionChartData($id){
        $questionData = DB::select("select user_answer, count(id) as answerCount from userquestion where question_id = '".$id."' group by user_answer;");
        return $questionData;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find($id);
        return $question;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'question_content' => 'required|string|max:255',
            'answer_type' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $product = Question::find($id);
        $product ->question_content = $request->question_content;
        $product ->answer_type = $request->answer_type;

        $product->save();

        $result = array("status"=>200,"message"=>"Question Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->delete();
    }
}
