<?php

namespace App\Http\Controllers;

use App\Receipt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $receipt = Receipt::all();
        return $receipt;
    }

    public function getUserReceipts($id){
        $receipt = Receipt::where("user_id",$id)->get();
        return $receipt;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|string',
            'receipt_image' => 'file|image|required',
            'merchant_name' => 'required|string|max:255',
            'total_amount' => 'required|numeric',
            'total_tax' => 'required|numeric',
            'receipt_data' => 'required|string',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $receipt = new Receipt;
        $path = $request->file('receipt_image')->store('receipt', 'public');
        $receipt ->category_id = $request->category_id;
        $receipt ->merchant_name = $request->merchant_name;
        $receipt ->total_amount = $request->total_amount;
        $receipt ->total_tax = $request->total_tax;
        $receipt ->receipt_data = $request->receipt_data;
        $receipt ->receipt_image = "storage/".$path;
        $receipt ->user_id = $request->user_id;

        $receipt->save();

        $result = array("status"=>200,"message"=>"Receipt Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receipt = Receipt::find($id);
        return $receipt;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function edit(Receipt $receipt)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|numeric',
            'receipt_image' => 'file|image|required',
            'merchant_name' => 'required|string|max:255',
            'total_amount' => 'required|numeric',
            'total_tax' => 'required|numeric',
            'receipt_data' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $receipt = Question::find($id);
        $path = $request->file('receipt_image')->store('receipt', 'public');
        $receipt ->category_id = $request->category_id;
        $receipt ->merchant_name = $request->merchant_name;
        $receipt ->total_amount = $request->total_amount;
        $receipt ->total_tax = $request->total_tax;
        $receipt ->receipt_data = $request->receipt_data;

        $receipt->save();

        $result = array("status"=>200,"message"=>"Receipt Updated", "data"=>"success" );
         return json_encode($result);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $receipt = Receipt::find($id);
        Storage::delete($receipt->receipt_image);
        $receipt->delete();
    }
}
