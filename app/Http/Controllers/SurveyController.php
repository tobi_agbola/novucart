<?php

namespace App\Http\Controllers;

use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Survey = Survey::all();
        return $Survey;
    }

    public function getUserSurveys($id){
        $survey = Survey::where("user_id",$id)->get();
        return $survey;
    }

    public function getSurveyAnswerCount($id){
        $surveyData = DB::select("select count(user_answer) as numberOfAnswers from surveys right join questions on surveys.id = questions.survey_id right join userquestion on questions.id = userquestion.question_id where surveys.id = '".$id."';");
        return $surveyData;
    }
    public function getUniqueUser($id){
        $surveyData = DB::select("select count(distinct(userquestion.user_id)) as uniqueUsers from surveys right join questions on surveys.id = questions.survey_id right join userquestion on questions.id = userquestion.question_id where surveys.id = '".$id."';");
        return $surveyData;
    }
    public function getAverageAnswerRate($id){
        $surveyData = DB::select("select (avg(answerRate) * 100) as averageAnswerRate from (select  (count(id)/(select count(id) from questions where survey_id = '".$id."')) as answerRate  from userquestion where user_id in (select distinct(user_id) from userquestion where question_id in (select id from questions where survey_id = '".$id."') group by user_id) group by user_id) as completionRateTable;");
        return $surveyData;
    }

    public function getSurveyChartData($id){
        $surveyData = DB::select("select user_answer, count(id) as answerCount from userquestion where question_id in (select id from questions where survey_id = '".$id."') group by user_answer;");
        return $surveyData;
    }

    public function getSurveyCSV($id){
        $surveyData = DB::select("select questions.question_content, questions.answer_type, questions.created_at as question_created_time, userquestion.user_answer, userquestion.created_at as answer_created_time from questions left join userquestion on questions.id = userquestion.question_id where survey_id = '".$id."';");
        return $surveyData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'survey_image' => 'file|image|required',
            'survey_name' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $survey = new Survey;
        $path = $request->file('survey_image')->store('survey', 'public');
        $survey ->survey_image = "storage/".$path;
        $survey ->survey_name = $request->survey_name;
        $survey ->user_id = $request->user_id;

        $survey->save();

        $result = array("status"=>200,"message"=>"Survey Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = Survey::find($id);
        return $survey;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'survey_name' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $survey = Survey::find($id);
        $survey ->survey_name = $request->survey_name;

        $survey->save();

        $result = array("status"=>200,"message"=>"Survey Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = Survey::find($id);
        $survey->delete();
    }
}
