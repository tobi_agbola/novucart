<?php

namespace App\Http\Controllers;

use App\UserQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class UserQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_question = UserQuestion::all();
        return $user_question;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'user_id' => 'required|string',
            'question_id' => 'required|string',
            'user_answer' => 'required|string',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $user_question = new UserQuestion;
        $user_question ->user_id = $request->user_id;
        $user_question ->question_id = $request->question_id;
        $user_question ->user_answer = $request->user_answer;

        $user_question->save();

        $result = array("status"=>200,"message"=>"User Question Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User_Question  $user_Question
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_question = UserQuestion::find($id);
        return $user_question;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User_Question  $user_Question
     * @return \Illuminate\Http\Response
     */
    public function edit(User_Question $user_Question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User_Question  $user_Question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            'user_id' => 'required|string',
            'question_id' => 'required|numeric',
            'user_answer' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $user_question = UserQuestion::find($id);
        $user_question ->user_id = $request->user_id;
        $user_question ->question_id = $request->question_id;
        $user_question ->user_answer = $request->user_answer;

        $user_question->save();

        $result = array("status"=>200,"message"=>"User Question Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User_Question  $user_Question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_question = UserQuestion::find($id);
        $user_question->delete();
    }
}
