<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video = Video::all();
        return $video;
    }

    public function getUserVideos($id){
        $video = Video::where("user_id",$id)->get();
        return $video;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $max_size = (int)ini_get('upload_max_filesize') * 1000;
        $validator = Validator::make($request->all(), [

            'genre_id' => 'required|string',
            'title' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'video_link' => 'file|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4|max:'.$max_size,
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $video = new Video;
        $path = $request->file('video_link')->store('video', 'public');
        $video ->genre_id = $request->genre_id;
        $video ->title = $request->title;
        $video ->author = $request->author;
        $video ->video_link = "storage/".$path;
        $video ->user_id = $request->user_id;

        $video ->save();

        $result = array("status"=>200,"message"=>"Video Added", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $videoResult = Video::find($id);
        return $videoResult;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'genre_id' => 'required|numeric',
            'title' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'video_link' => 'file|image|required',
        ]);

        if ($validator->fails()) {
             $result = array("status"=>200,"message"=>$validator->messages()->first(), "data"=>"error" );
             return json_encode($result);
        }

        $video = Video::find($id);
        $path = $request->file('video_link')->store('video', 'public');
        $video ->genre_id = $request->genre_id;
        $video ->title = $request->title;
        $video ->author = $request->author;
        $video ->video_link = $path;

        $video ->save();

        $result = array("status"=>200,"message"=>"Video Updated", "data"=>"success" );
         return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::find($id);
        Storage::delete($video->video_link);
        $video->delete();

    }
}
