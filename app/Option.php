<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use Models\UsesUuid;
    //
    public function questions()
    {
        return $this->belongsTo('App\Question');
    }
}
