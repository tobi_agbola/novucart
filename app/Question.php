<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use Models\UsesUuid;
    //
    public function surveys()
    {
        return $this->belongsTo('App\Survey');
    }

    public function options()
    {
        return $this->hasMany('App\Option');
    }

    public function userQuestions()
    {
        return $this->hasMany('App\UserQuestion');
    }
}
