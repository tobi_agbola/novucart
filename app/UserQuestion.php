<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestion extends Model
{
    use Models\UsesUuid;
    protected $table = 'userquestion';
    //
    public function questions()
    {
        return $this->belongsTo('App\Question');
    }
}
