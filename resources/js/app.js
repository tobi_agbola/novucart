
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('advert-component', require('./components/AdminAdvertComponent.vue').default);
Vue.component('category-component', require('./components/AdminCategoryComponent.vue').default);
Vue.component('genre-component', require('./components/AdminGenreComponent.vue').default);
Vue.component('product-component', require('./components/AdminProductComponent.vue').default);
Vue.component('receipt-component', require('./components/AdminReceiptComponent.vue').default);
Vue.component('question-component', require('./components/AdminQuestionComponent.vue').default);
Vue.component('survey-component', require('./components/AdminSurveyComponent.vue').default);
Vue.component('profile-component', require('./components/AdminProfileComponent.vue').default);
Vue.component('user-component', require('./components/AdminUserComponent.vue').default);
Vue.component('dashboard-component', require('./components/AdminDashboardComponent.vue').default);
Vue.component('video-component', require('./components/AdminVideoComponent.vue').default);
Vue.component('wallet-component', require('./components/AdminWalletComponent.vue').default);

Vue.component('company-dashboard-component', require('./components/CompanyDashboardComponent.vue').default);
Vue.component('company-profile-component', require('./components/CompanyProfileComponent.vue').default);
Vue.component('company-survey-component', require('./components/CompanySurveyComponent.vue').default);
Vue.component('company-report-component', require('./components/CompanyReportComponent.vue').default);
Vue.component('company-product-component', require('./components/CompanyProductComponent.vue').default);
Vue.component('company-video-component', require('./components/CompanyVideoComponent.vue').default);
Vue.component('company-advert-component', require('./components/CompanyAdvertComponent.vue').default);
Vue.component('survey-bar-graph-component', require('./components/surveyGraphBarComponent.vue').default);
Vue.component('survey-pie-graph-component', require('./components/surveyGraphPieComponent.vue').default);
Vue.component('question-bar-graph-component', require('./components/questionGraphBarComponent.vue').default);
Vue.component('question-pie-graph-component', require('./components/questionGraphPieComponent.vue').default);
Vue.component('survey-stats-component', require('./components/surveyStatisticsComponent.vue').default);
Vue.component('question-chart-component', require('./components/questionChartsComponent.vue').default);
Vue.component('admin-report-component', require('./components/AdminReportComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
