@extends('layouts.stucture-app')
@section('title','Dashboard')
@section('content')
    @component('layouts.header')
    @endcomponent
    <div class="container-fluid page-body-wrapper">
     @component('layouts.sidebar')
    @endcomponent
        <div class="main-panel">
            <div class="content-wrapper">
                    <div id="app">
                        <genre-component></genre-component>
                        @csrf
                    </div>
                    <input type="hidden" id="user_id" value="{{ Auth::user()->id }}" />
            </div>
        </div>
    </div>
    @component('layouts.footer')
    @endcomponent
@endsection
