<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="text-wrapper">
                  <p class="profile-name">{{ Auth::user()->name }}</p>
                </div>
              </div>
            </div>
          </li>
            @if (Auth::user()->role == "admin")
                <li class="nav-item">
                <a class="nav-link" href="{{route('home')}}">
                    <i class="menu-icon mdi mdi-television"></i>
                    <span class="menu-title">Admin Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <i class="menu-icon mdi mdi-content-copy"></i>
                    <span class="menu-title">Surveys</span>
                    <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('adminsurvey')}}">Survey</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('adminquestion')}}">Questions</a>
                        </li>
                    </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admincategory')}}">
                    <i class="menu-icon mdi mdi-backup-restore"></i>
                    <span class="menu-title">Categories</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('adminproduct')}}">
                    <i class="menu-icon mdi mdi-chart-line"></i>
                    Products
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('adminadvert')}}">
                    <i class="menu-icon mdi mdi-table"></i>
                    <span class="menu-title">Adverts</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('adminreport') }}">
                    <i class="menu-icon mdi mdi-chart-line"></i>
                    <span class="menu-title">Reports</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admingenre')}}">
                    <i class="menu-icon mdi mdi-sticker"></i>
                    <span class="menu-title">Genre</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('adminreceipt')}}">
                    <i class="menu-icon mdi mdi-sticker"></i>
                    <span class="menu-title">Reciept</span>
                    </a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="{{route('adminwallet')}}">
                        <i class="menu-icon mdi mdi-content-copy"></i>
                        <span class="menu-title">Wallet</span>
                        </a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="{{route('adminvideo')}}">
                        <i class="menu-icon mdi mdi-sticker"></i>
                        <span class="menu-title">Video</span>
                        </a>
                </li>
            @endif
            @if (Auth::user()->role == "company")
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">
                    <i class="menu-icon mdi mdi-television"></i>
                    <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('companyadvert')}}">
                    <i class="menu-icon mdi mdi-table"></i>
                    <span class="menu-title">Adverts</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('companyproduct')}}">
                    <i class="menu-icon mdi mdi-chart-line"></i>
                    Products
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('companysurvey') }}">
                    <i class="menu-icon mdi mdi-backup-restore"></i>
                    <span class="menu-title">Surveys</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('companyreport') }}">
                    <i class="menu-icon mdi mdi-chart-line"></i>
                    <span class="menu-title">Reports</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('companyvideo')}}">
                    <i class="menu-icon mdi mdi-sticker"></i>
                    <span class="menu-title">Video</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('companyprofile') }}">
                    <i class="menu-icon mdi mdi-sticker"></i>
                    <span class="menu-title">Profile</span>
                    </a>
                </li>
            @endif
          <li class="nav-item">
              <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="menu-icon mdi mdi-restart"></i> {{ __('Logout') }} </a>
          </li>
        </ul>
      </nav>
