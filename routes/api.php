<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('advert', 'AdvertController');
Route::get('advert/{id}/user','AdvertController@getUserAdverts');
Route::resource('category', 'CategoryController');
Route::get('category/{id}/user','CategoryController@getUserCategorys');
Route::get('option/{id}/question','OptionController@getOptionsFromQuestionId');
Route::resource('option', 'OptionController');
Route::resource('product', 'ProductController');
Route::get('product/{id}/user','ProductController@getUserProducts');
Route::resource('question', 'QuestionController');
Route::get('question/{id}/user','QuestionController@getUserQuestions');
Route::get('question/{id}/survey','QuestionController@getSurveyQuestions');
Route::get('question/{id}/chart','QuestionController@getQuestionChartData');
Route::resource('receipt', 'ReceiptController');
Route::get('receipt/{id}/user','ReceiptController@getUserReceipts');
Route::resource('survey', 'SurveyController');
Route::get('survey/{id}/user','SurveyController@getUserSurveys');
Route::get('survey/{id}/report','SurveyController@getSurveyAnswerCount');
Route::get('survey/{id}/answer','SurveyController@getAverageAnswerRate');
Route::get('survey/{id}/chart','SurveyController@getSurveyChartData');
Route::get('survey/{id}/uniqueUsers','SurveyController@getUniqueUser');
Route::get('survey/{id}/csv','SurveyController@getSurveyCSV');
Route::resource('userQuestion', 'UserQuestionController');
Route::resource('video', 'VideoController');
Route::get('video/{id}/user','VideoController@getUserVideos');
Route::resource('genre', 'GenreController');
Route::get('genre/{id}/user','GenreController@getUserGenre');
Route::resource('wallet', 'WalletController');
Route::get('wallet/{id}/user','WalletController@getUserWallets');
