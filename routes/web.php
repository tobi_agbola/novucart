<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', function () {
    if(Auth::user()->role == "admin"){
        return view('adminDashboard');
    }else{
        return view('companyDashboard');
    }

});
Route::get('/adminadvert', function () { return view('adminAdvert'); })->name('adminadvert')->middleware('auth');

Route::get('/admincategory', function () { return view('adminCategory'); })->name('admincategory')->middleware('auth');

Route::get('/admingenre', function () { return view('adminGenre'); })->name('admingenre')->middleware('auth');

Route::get('/adminproduct', function () { return view('adminProduct'); })->name('adminproduct')->middleware('auth');

Route::get('/adminquestion', function () { return view('adminQuestion'); })->name('adminquestion')->middleware('auth');

Route::get('/adminreceipt', function () { return view('adminReceipt'); })->name('adminreceipt')->middleware('auth');

Route::get('/adminsurvey', function () { return view('adminSurvey'); })->name('adminsurvey')->middleware('auth');

Route::get('/adminwallet', function () { return view('adminWallet'); })->name('adminwallet')->middleware('auth');

Route::get('/adminvideo', function () { return view('adminVideo'); })->name('adminvideo')->middleware('auth');

Route::get('/adminreport', function () { return view('adminReport'); })->name('adminreport')->middleware('auth');

Route::get('/profile', function () { return view('companyProfile'); })->name('companyprofile')->middleware('auth');

Route::get('/report', function () { return view('companyReport'); })->name('companyreport')->middleware('auth');

Route::get('/survey', function () { return view('companySurvey'); })->name('companysurvey')->middleware('auth');

Route::get('/video', function () { return view('companyVideo'); })->name('companyvideo')->middleware('auth');

Route::get('/product', function () { return view('companyProduct'); })->name('companyproduct')->middleware('auth');

Route::get('/advert', function () { return view('companyAdvert'); })->name('companyadvert')->middleware('auth');
